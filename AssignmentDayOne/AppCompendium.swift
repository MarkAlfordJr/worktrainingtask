//
//  AppCompendium.swift
//  AssignmentDayOne
//
//  Created by Mark Alford on 8/10/21.
//

import Foundation

//MARK: - Requirements
//Create a single view app with 3 text fields each for
    //Name,
    //age,
    //gender as enum
//button to add input to an Object array, Person(name,age,gender)

//Write 4 functions to return the following,
    // Sorted alphabetically
    // Younger to older objects
    // dictionary with keys
        //kids(1-17),
        //young adults(18-35),
        //adults (36-55),
        //senior (>55) values being list of person accordingly.
    //Accepts parameters 'indexes' , returns Dictionary pairs of 'index' key and 'Person tuples' values from alphabetically sorted list

//Considerations:
//If age is not entered by user keep it nil
//Male/Female should be stored as enum
//No need to spend much time on UI.
//Consider possible negative scenarios and handle them accordingly

//MARK: - Design
//Tech Stacks
    //TextField Delegate
//Pattern
    //MVC

//MARK: - Implementation
//Stages
    //1 setup storyboard - done
    //2 setup elements with code - done
    //3 setup MVC file structure - done
    //4 make objects/tuples with user input and append to array, print - done
    //5 make functions that manipulate filled array, print results

