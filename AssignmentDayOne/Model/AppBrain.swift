//
//  AppBrain.swift
//  AssignmentDayOne
//
//  Created by Mark Alford on 8/10/21.
//

import Foundation

struct AppBrain {
    //MARK: - Properties
    
    //establish the array that will contain user's Person Tuples
    typealias Event = (name: String, age: String, gender: String)
    var userArray:[Event] = []
    

    //MARK: - Main Functionality
    
    //function where, VC textfield values fill up a tuple, to append to userArray
    mutating func tupleAddToArray (userName: String, userAge: String?, userGender: String) {
        if let age = userAge {
            userArray.append((name: userName, age: age, gender: userGender))
            print(userArray)
        } else {
            userArray.append((name: userName, age: "0", gender: userGender))
            print(userArray)
        }
        
    }
    
    
    
    
    //MARK: - sort alphabetically
    
    // Sorted alphabetically
    func sortByName () {
        let sortedNameTuples = userArray.sorted(by: {$0.name < $1.name})
        print("Sorted Name")
        print(sortedNameTuples)
    }
    
    
    //MARK: - younger to older
    // Younger to older objects
    func sortedByAge () {
        let sortedAgeTuples = userArray.sorted(by: {$0.age < $1.age})
        print("Sorted Age")
        print(sortedAgeTuples)
    }
    
    
    //MARK: - sort into dictionary
    // dictionary with keys
        //kids(1-17),
        //young adults(18-35),
        //adults (36-55),
        //senior (>55)
    //values being list of person accordingly.
    func sortIntoDictionary() {
        print("Sorted Dictionary")
        //make dictionary with string key, and array of tuples value
        var sortedDict: [String : [(name: String, age: String, gender: String)]] = [:]
        //loop through userArray
        for tupleItem in userArray {
            //switch userArray tuple item
            switch Int(tupleItem.age) ?? 0 {
            //case equals 1...17
            case 1...17:
                //get access to kid key of dictionary
                //have kid key append userArray tuple into dictionary
                _ = sortedDict.append(element: tupleItem, toValueOfKey: "Kid")
                print(sortedDict)
            //case equals 18...35
            case 18...35:
                //get access to kid key of dictionary
                //have kid key append userArray tuple into dictionary
                _ = sortedDict.append(element: tupleItem, toValueOfKey: "Young Adult")
                print(sortedDict)
            //case equals 36...55
            case 36...55:
                //get access to kid key of dictionary
                //have kid key append userArray tuple into dictionary
                _ = sortedDict.append(element: tupleItem, toValueOfKey: "Adult")
                print(sortedDict)
            //case equals 55...
            case 55...:
                //get access to senior key of dictionary
                //have senior key append userArray tuple into dictionary
                _ = sortedDict.append(element: tupleItem, toValueOfKey: "Senior")
                print(sortedDict)
            case nil:
                _ = sortedDict.append(element: tupleItem, toValueOfKey: "No-Age")
                print(sortedDict)
            default:
                print("no functionality")
            }
        }
        
    }
    

    //MARK: - returns dictionary
    //Accepts parameters 'indexes' , returns Dictionary pairs of 'index' key and 'Person tuple' values from alphabetically sorted list
    func indexedDict (index: Int) -> Dictionary<Int, (name: String, age: String, gender: String)> {
        print("Indexed Dictionary")
        //sort tuples alphabetically
        let sortedTuples = userArray.sorted(by: {$0.name < $1.name})
        //given parameter index (which may equal userArray.count), show dictionary
        var newDict: [Int : (name: String, age: String, gender: String)] = [:]
        //loop through sorted tuples array
        for tuple in sortedTuples{
            //setup a numbered key (which will be index parameter)
            //have each key's value be a tuple from the sorted list
            newDict[index] = tuple
        }
        
        
        //return dictionary
        return newDict
    }
   
    
    
}



//MARK: - Dictionary Sort Extension
extension Dictionary where Value: RangeReplaceableCollection {
    
    public mutating func append(element: Value.Iterator.Element, toValueOfKey key: Key) -> Value? {
        var value: Value = self[key] ?? Value()
        value.append(element)
        self[key] = value
        return value
    }
}
