//
//  ViewController.swift
//  AssignmentDayOne
//
//  Created by Mark Alford on 8/10/21.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - UI Elements
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    
    //acces to model struct
    var appBrain = AppBrain()

    
    //MARK: - Action
    @IBAction func createPersonBtn(_ sender: UIButton) {
        
        //if 2 values of gender, perform action
        if genderTextField.text == "male"{
            appBrain.tupleAddToArray(userName: nameTextField.text!, userAge: ageTextField.text ?? "0" , userGender: genderTextField.text!)
        } else if genderTextField.text == "female" {
            appBrain.tupleAddToArray(userName: nameTextField.text!, userAge: ageTextField.text ?? "0" , userGender: genderTextField.text!)
        }
        
        //call 4 functions
        appBrain.sortByName()
        appBrain.sortedByAge()
        appBrain.sortIntoDictionary()
        let sortAbDict = appBrain.indexedDict(index: appBrain.userArray.count)
        print(sortAbDict)
        
        //restart with textfields begin empty
        ageTextField.text = ""
        nameTextField.text = ""
        genderTextField.text = ""
        
    }
    
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //set gender textfield to zero
        genderTextField.autocapitalizationType = UITextAutocapitalizationType.none
    }
    
    //MARK: - Delegate Functionality
    
    //when enter button is pressed on keypad, execute code
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === nameTextField {
            // do something to nameText
            nameTextField.endEditing(true)
            print(nameTextField.text ?? "nothing")
        } else if textField === ageTextField {
            // do something to definitionTextView
            ageTextField.endEditing(true)
            print(ageTextField.text ?? "nothing, or 0")
        } else if textField === genderTextField{
            // handle other text views
            genderTextField.endEditing(true)
            print(genderTextField.text ?? "nothing")
        }
        return true
    }
    
    //if textfield has no input in it, when enter button is pressed, textfield will tell user to type first.
    //this blocks the 'no input to begin with' error when user doesnt even type anything in textfield.
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField === nameTextField {
            // do something to nameText
            if nameTextField.text != ""{
                return true
            } else {
                nameTextField.placeholder = "please enter your name"
                return false
            }
        } else if textField === ageTextField {
            // do something to definitionTextView
            if ageTextField.text != ""{
                return true
            } else {
                ageTextField.text = String(0)
                return false
            }
        } else if textField === genderTextField{
            // handle other text views
            if genderTextField.text != ""{
                return true
            } else {
                genderTextField.placeholder = "please enter your gender"
                return false
            }
        } else {
            return false
        }
        
    }
    
    
    //when the textfield stops editing officially, done by enter button on keypad
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField === nameTextField {
            // do something to nameText
            nameTextField.endEditing(true)
            print(nameTextField.text ?? "nothing")
        } else if textField === ageTextField {
            // do something to definitionTextView
            ageTextField.endEditing(true)
            print(ageTextField.text ?? "nothing or 0")
        } else if textField === genderTextField{
            // handle other text views
            genderTextField.endEditing(true)
            print(genderTextField.text ?? "nothing")
        }
    }
    
    //TextField Lowercase Enabler
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if let _ = string.rangeOfCharacter(from: .uppercaseLetters) {
            // Do not allow upper case letters
            return false
        }
        return true
    }
    
}

